
import 'package:overone/src/screens/elements/underlined_long_button.dart';
import 'package:overone/src/screens/home/sub_categories.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared/modules/common/models/category.dart';
import 'package:shared/modules/content/bloc/bloc_controller.dart';
import 'package:shared/modules/content/bloc/content/content_bloc.dart';
import 'package:shared/modules/content/bloc/content/content_bloc_public.dart';
import 'package:shared/modules/content/bloc/content/content_state.dart';

class CategoriesList extends StatefulWidget {
  @override
  State<CategoriesList> createState() => _CategoriesListState();
}

class _CategoriesListState extends State<CategoriesList> {
  final ContentBloc contentBloc = ContentBlocController().contentBloc;
  List<CategoryInfo> items = [];

  @override
  void initState() {
    super.initState();
    this.contentBloc.add(GetCategories());
  }

  void updateState(ContentState state) {
    if (state is CategoriesBecome) {
      items = state.categories;
    }
  }

  @override
  Widget build(BuildContext context) {
    var headerTitle = 'Категории';
    var allWidth = MediaQuery.of(context).size.width;
    var titleSize = allWidth * 0.055;

    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: false,
            iconTheme: IconThemeData(
              color: Color(0xff151515),
            ),
            backgroundColor: Color(0xfff4f4f4),
            elevation: 1,
            title: Text(
              headerTitle,
              style: TextStyle(
                  color: Color(0xff151515),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Helvetica Neue',
                  fontStyle: FontStyle.normal,
                  fontSize: titleSize
              ),
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(12),
              ),
            )
        ),
        body: BlocBuilder<ContentBloc, ContentState>(
          cubit: this.contentBloc,
          builder: (BuildContext context, ContentState state) {
            updateState(state);

            var allWidth = MediaQuery.of(context).size.width;
            var bigTextSize = allWidth * 0.05;
            var mainTextSize = allWidth * 0.038;
            var smallTextSize = allWidth * 0.030;

            return Column(
              children: [
                Expanded(
                  child: ListView.builder(
                    itemCount: this.items.length,
                    itemBuilder: (context, index) {
                      CategoryInfo item = this.items.elementAt(index);
                      return InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => new SubCategoriesList(
                                      parentCategory: item,
                                  )
                              ));
                        },
                        child: UnderlinedLongButton(
                          titleWidget: Column(
                              children: [
                                SizedBox(height: allWidth * 0.022),
                                Align(
                                    alignment: Alignment.topLeft,
                                    child: Text(
                                      item.title,
                                      style: TextStyle(
                                        fontFamily: 'Helvetica Neue',
                                        fontSize: mainTextSize,
                                        fontWeight: FontWeight.w500,
                                        color: Color(0xff151515),
                                      ),
                                      maxLines: 1,
                                    )
                                ),
                                SizedBox(height: allWidth * 0.03),
                                Align(
                                    alignment: Alignment.bottomLeft,
                                    child: Text(
                                      '${item.categoriesCount} подкатегорий',
                                      style: TextStyle(
                                        fontFamily: 'Helvetica Neue',
                                        fontSize: smallTextSize,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xff808486),
                                      ),
                                      maxLines: 1,
                                    )
                                )
                              ]
                          ),
                          functionalBlockWidth: allWidth * 0.155,
                          functionalBlock: Container(
                            width: allWidth * 0.155,
                            child: Center(
                              child: Icon(
                                Icons.arrow_forward_ios,
                                size: bigTextSize,
                                color: Color(0xffbbbfc0),
                              ),
                            )
                          ),
                          height: allWidth * 0.155,
                        ),
                      );
                    },
                  ),
                ),
              ],
            );
          }
        )
    );
  }
}
