
import 'package:overone/src/screens/elements/underlined_long_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared/main.dart';
import 'package:shared/modules/authentication/bloc/authentication/authentication_bloc.dart';
import 'package:shared/modules/authentication/bloc/bloc_controller.dart';
import 'package:shared/modules/authentication/models/current_user_data.dart';

class ProfileInfo extends StatefulWidget {
  @override
  State<ProfileInfo> createState() => _ProfileInfoState();
}

class _ProfileInfoState extends State<ProfileInfo> {
  CurrentUserData? userData;
  final AuthenticationBloc authenticationBloc =
      AuthenticationBlocController().authenticationBloc;

  Widget getButton(String text) {
    var allWidth = MediaQuery.of(context).size.width;
    var bigTextSize = allWidth * 0.05;
    var mainTextSize = allWidth * 0.038;

    return Container(
        color: Color(0xffffffff),
        child: UnderlinedLongButton(
          functionalBlockWidth: allWidth * 0.155,
          functionalBlock: Container(
              width: allWidth * 0.155,
              child: Center(
                child: Icon(
                  Icons.arrow_forward_ios,
                  size: bigTextSize,
                  color: Color(0xffbbbfc0),
                ),
              )
          ),
          titleWidget: Container(
              child: Align(
                  alignment: Alignment.centerLeft,
                  child: Text(
                    text,
                    style: TextStyle(
                      fontFamily: 'Helvetica Neue',
                      fontSize: mainTextSize,
                      fontWeight: FontWeight.w400,
                      color: Color(0xff151515),
                    ),
                    maxLines: 1,
                  )
              )
          ),
          height: allWidth * 0.122,
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    authenticationBloc.add(GetUserData());
    var allWidth = MediaQuery.of(context).size.width;
    var titleSize = allWidth * 0.055;
    var mainTextSize = allWidth * 0.038;
    var avatarWidth = allWidth * 0.27;

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xfff4f4f4),
        elevation: 1,
        iconTheme: IconThemeData(
          color: Color(0xff151515),
        ),
        title: Text('Профиль',
          style: TextStyle(
              color: Color(0xff151515),
              fontWeight: FontWeight.w500,
              fontFamily: 'Helvetica Neue',
              fontStyle: FontStyle.normal,
              fontSize: titleSize
          ),
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(12),
          ),
        ),
      ),
      body: WillPopScope(
        onWillPop: () async => false,
        child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
          cubit: authenticationBloc,
          builder: (BuildContext context, AuthenticationState state) {
            if (state is SetUserData) {
              this.userData = state.currentUserData;
            }

            if (this.userData == null) {
              return Center(
                child: Container(
                  height: 70,
                  color: Colors.transparent,
                  child: new CircularProgressIndicator(),
                ),
              );
            }
            String? image = this.userData?.data.avatar;
            String? login = this.userData?.data.login;
            var hasImage = image != "";

            return Container(
              color: Color(0xfff4f4f4),
              child: ListView(
                children: [
                  Container(
                    width: allWidth,
                    height: allWidth * 0.44,
                    color: Color(0xffffffff),
                    child: Column(
                      children: [
                        SizedBox(height: allWidth * 0.04),
                        Container(
                            width: avatarWidth,
                            height: avatarWidth,
                            child: Visibility(
                                visible: hasImage,
                                child: AspectRatio(
                                    aspectRatio: 1,
                                    child: Container(
                                        decoration: BoxDecoration(
                                            borderRadius: BorderRadius.circular(avatarWidth * 0.5),
                                            image: DecorationImage(
                                              fit: BoxFit.cover,
                                              alignment: FractionalOffset.center,
                                              image: NetworkImage(image!),
                                            )
                                        )
                                    )
                                )
                            )
                        ),
                        SizedBox(height: allWidth * 0.04),
                        Container(
                          width: avatarWidth,
                          child: Center(
                            child: Text(
                              login!,
                              style: TextStyle(
                                  fontFamily: 'Helvetica Neue',
                                  fontSize: mainTextSize,
                                  fontWeight: FontWeight.w500,
                                  color: Color(0xff151515)
                              )
                            )
                          )
                        )
                      ],
                    )
                  ),
                  SizedBox(height: allWidth * 0.022),
                  Container(
                    padding: EdgeInsets.fromLTRB(allWidth * 0.044, 0, 0, 0),
                    height: allWidth * 0.127,
                    width: allWidth,
                    child: Container(
                      child: Align(
                        alignment: Alignment.centerLeft,
                        child: Text(
                          'Отображение контента в ленте',
                          style: TextStyle(
                              fontFamily: 'Helvetica Neue',
                              fontSize: mainTextSize,
                              fontWeight: FontWeight.w500,
                              color: Color(0xff808486)
                          )
                        )
                      )
                    )
                  ),
                  Container(
                    width: allWidth,
                    height: allWidth * 0.32,
                    color: Color(0xffffffff),
                    child: Center(
                      child: Text('TODO',
                        style: TextStyle(
                          color: Colors.red,
                          fontWeight: FontWeight.w800,
                          fontSize: mainTextSize
                        )
                      )
                    )
                  ),
                  SizedBox(height: allWidth * 0.022),
                  getButton('Редактировать профиль'),
                  getButton('Настройки'),
                  getButton('ПО'),
                  InkWell(
                    onTap: () {
                      setState(() {
                        authenticationBloc.add(UserLogOut());
                      });
                    },
                    child: getButton('Выйти'),
                  ),
                ],
              )
            );
          }
        )
      )
    );
  }
}
