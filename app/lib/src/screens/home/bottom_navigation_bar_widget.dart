
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../pages_cache.dart';

/// This is the stateful widget that the main application instantiates.
class BottomNavigationBarWidget extends StatefulWidget {
  const BottomNavigationBarWidget({required Key? key, required this.pagesCache}) : super(key: key);

  final PagesCache pagesCache;

  @override
  State<BottomNavigationBarWidget> createState() =>
      _BottomNavigationBarWidgetState();
}

/// This is the private State class that goes with MyStatefulWidget.
class _BottomNavigationBarWidgetState extends State<BottomNavigationBarWidget> {
  late List<Widget> screens;
  int _selectedIndex = 0;

  @override
  void initState() {
    screens = [
      this.widget.pagesCache.lentaPage,
      this.widget.pagesCache.articlesScreen,
      this.widget.pagesCache.categoriesPage,
      this.widget.pagesCache.foldersPage,
      this.widget.pagesCache.profilePage,
    ];
    super.initState();
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      build(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    var allWidth = MediaQuery.of(context).size.width;
    var textSize = allWidth * 0.033;
    var bottomIconsSize = allWidth * 0.08;

    return Scaffold(
      // body:IndexedStack(
      //   index: _selectedIndex,
      //   children: screens,
      // ),
      body: Center(
        child: screens.elementAt(_selectedIndex),
      ),
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,

        currentIndex: _selectedIndex,
        unselectedItemColor: Color(0xff808486),
        showUnselectedLabels: false,
        selectedItemColor: Color(0xff151515),
        backgroundColor: Color(0xfff4f4f4),
        // elevation: 1,
        selectedFontSize: textSize,
        iconSize: bottomIconsSize,
        items: [
          BottomNavigationBarItem(
            label: "ЛЕНТА",
            icon: Icon(Icons.art_track),
          ),
          BottomNavigationBarItem(
            label: "ИЗБРАННОЕ",
            icon: Icon(Icons.turned_in_not),
          ),
          BottomNavigationBarItem(
            label: "КАТЕГОРИИ",
            icon: Icon(Icons.add),
          ),
          BottomNavigationBarItem(
            label: "ПАПКИ",
            icon: Icon(Icons.folder_open),
          ),
          BottomNavigationBarItem(
            label: "ПРОФИЛЬ",
            icon: Icon(Icons.account_circle_outlined),
          ),
        ],
        onTap: _onItemTapped,
      ),
    );
  }
}

