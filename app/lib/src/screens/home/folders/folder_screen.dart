
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared/modules/common/models/feed_list_data.dart';
import 'package:shared/modules/common/models/news_list_data.dart';

import '../some_lenta.dart';

class FolderScreen extends StatefulWidget {
  FolderScreen({
    required this.title,
    required this.iconData,
    required this.folderId,
    this.newsListType = ListTypes.folderlenta,
    this.feedsListType = FeedListTypes.folderfeeds,
  });

  String title;
  IconData iconData;
  int folderId;
  ListTypes newsListType;
  FeedListTypes feedsListType;

  @override
  State<StatefulWidget> createState() => _FolderScreenState();
}

class _FolderScreenState extends State<FolderScreen>
    with SingleTickerProviderStateMixin {
  final List<Tab> tabs = <Tab>[
    Tab(text: 'НОВОСТИ'),
    Tab(text: 'ИСТОЧНИКИ'),
  ];

  List<SomeLenta> pages = [];
  late TabController _tabController;

  @override
  void initState() {
    super.initState();

    pages = <SomeLenta>[
      // Articles.
      SomeLenta(
        newsListInfo: NewsListData(
          type: widget.newsListType,
          folderId: widget.folderId,
        ),
        feedListInfo: FeedListData(
          type: FeedListTypes.none,
        ),
      ),
      // Sources.
      SomeLenta(
        newsListInfo: NewsListData(
          type: ListTypes.none,
        ),
        feedListInfo: FeedListData(
          type: widget.feedsListType,
          folderId: widget.folderId,
        ),
        folderId: this.widget.folderId,
      ),
    ];

    _tabController = TabController(
        vsync: this,
        length: tabs.length,
        initialIndex: 0
    );
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var allWidth = MediaQuery.of(context).size.width;
    var textSize = allWidth * 0.033;
    var logoSize = allWidth * 0.055;
    var bigTextSize = allWidth * 0.055;

    return Scaffold(
      // bottomNavigationBar: BottomNavigationBarWidget(),
      appBar: AppBar(
        automaticallyImplyLeading: true,
        iconTheme: IconThemeData(
          color: Color(0xff151515),
        ),
        backgroundColor: Color(0xfff4f4f4),
        elevation: 1,
        title: Row(
          children: [
            Icon(
              widget.iconData,
              size: bigTextSize,
              color: Color(0xff808486),
            ),
            SizedBox(width: allWidth * 0.02),
            Text(widget.title,
              style: TextStyle(
                  color: Color(0xff151515),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Helvetica Neue',
                  fontStyle: FontStyle.normal,
                  fontSize: logoSize
              ),
            ),
          ],
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.vertical(
            bottom: Radius.circular(12),
          ),
        ),
        bottom: TabBar(
          indicatorColor: Color(0xff0A93A6),
          indicatorSize: TabBarIndicatorSize.label,
          labelColor: Color(0xff151515),
          unselectedLabelColor: Color(0xff808486),

          labelStyle: TextStyle(
              fontSize: textSize,
              fontWeight: FontWeight.w500,
              fontFamily: 'Helvetica Neue'),
          unselectedLabelStyle: TextStyle(
              fontSize: textSize,
              fontWeight: FontWeight.w500,
              fontFamily: 'Helvetica Neue'),
          controller: _tabController,
          tabs: tabs,
        ),
      ),
      body: WillPopScope(
        onWillPop: () async {
          Navigator.pop(context, false);
          return false;
        },
        child: Container(
          padding: EdgeInsets.zero,
          child: TabBarView(
            controller: _tabController,
            children: pages,
          ),
        )
      )
    );
  }
}
