
import 'package:overone/src/screens/elements/underlined_long_button.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared/modules/common/models/folder_info.dart';
import 'package:shared/modules/content/bloc/bloc_controller.dart';
import 'package:shared/modules/content/bloc/content/content_bloc.dart';
import 'package:shared/modules/content/bloc/content/content_event.dart';
import 'package:shared/modules/content/bloc/content/content_state.dart';

import 'create_edit_folder.dart';

class FolderSelection extends StatefulWidget {
  FolderSelection({required this.feedId});

  final ContentBloc contentBloc = ContentBlocController().contentBloc;
  List<FolderInfo> items = [];
  List<int> selectedFolders = [];
  final int feedId;

  @override
  State<FolderSelection> createState() => _FolderSelectionState();
}

class _FolderSelectionState extends State<FolderSelection> {

  @override
  void initState() {
    super.initState();
    this.widget.contentBloc.add(GetFolders());
  }

  void updateState(List<FolderInfo> folders) {
    // TODO: Some states emits multiple times. That's not good, figure out what the deal.
    var set = Set.from(folders);
    folders = List.from(set);
    folders.sort((a, b) => a.folderPosition.compareTo(b.folderPosition));
    widget.items = folders;
  }

  UnderlinedLongButton getLongButton(IconData iconData, String title,
      double functionalBlockWidth, Widget functionalBlock) {
    var allWidth = MediaQuery.of(context).size.width;
    var mainTextSize = allWidth * 0.038;

    return UnderlinedLongButton(
      decorationLeftIcon: iconData,
      isDecorationIconVisible: true,
      titleWidget: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            title,
            style: TextStyle(
              fontFamily: 'Helvetica Neue',
              fontSize: mainTextSize,
              fontWeight: FontWeight.w500,
              color: Color(0xff151515),
            ),
            maxLines: 1,
          )
      ),
      functionalBlockWidth: functionalBlockWidth,
      functionalBlock: functionalBlock,
      height: allWidth * 0.111,
      isUsePadding: false,
    );
  }

  bool isIdFolderChecked(int folderId) {
    return this.widget.selectedFolders.firstWhere((element) =>
      (element == folderId), orElse: () => -1) != -1;
  }
  
  void subscribeToSources() {
    this.widget.contentBloc.add(
        SubscribeActions(
          doSubscribe: true,
          feedId: this.widget.feedId,
          folderIds: this.widget.selectedFolders)
    );
  }

  @override
  Widget build(BuildContext context) {
    var headerTitle = 'Выбор папки';
    var allWidth = MediaQuery.of(context).size.width;
    var titleSize = allWidth * 0.055;
    var bigTextSize = allWidth * 0.05;

    return Scaffold(
        appBar: AppBar(
            automaticallyImplyLeading: true,
            iconTheme: IconThemeData(
              color: Color(0xff151515),
            ),
            backgroundColor: Color(0xfff4f4f4),
            elevation: 1,
            title: Text(
              headerTitle,
              style: TextStyle(
                  color: Color(0xff151515),
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Helvetica Neue',
                  fontStyle: FontStyle.normal,
                  fontSize: titleSize
              ),
            ),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.vertical(
                bottom: Radius.circular(12),
              ),
            ),
            actions: <Widget>[
              TextButton(
                  onPressed: (){
                    showDialog<void>(
                        context: context,
                        builder: (context) => CreateEditFolderDialog(
                            FolderDialogType.create,
                            allWidth,
                            null,
                            (String text) {
                              this.widget.contentBloc.add(CreateUserFolder(
                                  title: text,
                                  iconName: "folder",
                                  isPublic: false)
                              );
                              Navigator.pop(context);
                            },
                            () {
                              Navigator.pop(context);
                            }
                        )
                    );
                  },
                  child: Text(
                    'СОЗДАТЬ  ',
                    style: TextStyle(
                      fontFamily: 'Helvetica Neue',
                      fontSize: allWidth * 0.033,
                      color: Color(0xff0a93a6),
                      fontWeight: FontWeight.w500,
                    ),
                  ))
            ]
        ),
        body: BlocBuilder<ContentBloc, ContentState>(
            cubit: this.widget.contentBloc,
            builder: (BuildContext context, ContentState state) {
              if (state is FoldersBecome) {
                updateState(state.folders);
              } else if (state is FolderDeleted) {
                if (state.success) {
                  var folderId = state.folderId;
                  this.widget.items.removeWhere((element) => element.id == folderId);
                }
              } else if (state is FolderInfoUpdated) {
                var folderId = state.folder.id;
                var pos = this.widget.items.indexWhere((element) => element.id == folderId);
                this.widget.items[pos] = state.folder;
              } else if (state is FolderInfoCreated) {
                var folders = this.widget.items;
                folders.add(state.folder);
                updateState(folders);
              } else if (state is SubscribeChanged) {
                var feed = state.singleFeed;
                if (feed.items.isEmpty) {
                  // TODO: Catch this or do something else.
                  throw "Unreachable";
                } else if (feed.items.first.id == this.widget.feedId) {
                  Navigator.pop(context);
                  return Container();
                }
              }

              return Column(
                children: [
                  getLongButton(Icons.folder_outlined,
                    'Все подписки',
                    allWidth * 0.111,
                    Icon(
                      Icons.check,
                      size: bigTextSize,
                      color: Color(0xffbbbfc0),
                    )
                  ),
                  SizedBox(height: allWidth * 0.027),
                  Expanded(
                    child: ListView.builder(
                      itemCount: this.widget.items.length,
                      itemBuilder: (context, index) {
                        FolderInfo item = this.widget.items.elementAt(index);
                        return InkWell(
                            onTap: () {
                              setState(() {
                                if (isIdFolderChecked(item.id))
                                  this.widget.selectedFolders.removeWhere((
                                      element) => element == item.id);
                                else
                                  this.widget.selectedFolders.add(item.id);
                              });
                            },
                            child: getLongButton(
                              Icons.folder_outlined,
                              item.title,
                              allWidth * 0.111,
                              Visibility(
                                visible: isIdFolderChecked(item.id),
                                child: Icon(
                                  Icons.check,
                                  size: bigTextSize,
                                  color: Color(0xff0a93a6),
                                ),
                              ),
                            )
                        );
                      },
                    ),
                  ),
                  Container(
                    height: allWidth * 0.155,
                    width: allWidth,
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(
                          color: Color(0xffE8E8E8),
                          width: 1.5,
                        ),
                      ),
                    ),
                    child: TextButton(
                      onPressed: () {
                        this.subscribeToSources();
                      },
                      child: Center(
                          child: Text(
                            'ГОТОВО',
                            style: TextStyle(
                              fontFamily: 'Helvetica Neue',
                              fontSize: allWidth * 0.033,
                              color: Color(0xff0a93a6),
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                    ),
                  ),
                ],
              );
            }
        )
    );
  }
}
