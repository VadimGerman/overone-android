// home screen contents
import 'package:overone/src/screens/home/main_lenta.dart';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared/main.dart';

import '../pages_cache.dart';
import 'bottom_navigation_bar_widget.dart';

class HomeScreen extends StatelessWidget {
  final AuthenticationBloc authenticationBloc =
      AuthenticationBlocController().authenticationBloc;

  @override
  Widget build(BuildContext context) {
    authenticationBloc.add(GetUserData());
    return WillPopScope(
        onWillPop: () async => false,
        child: BlocBuilder<AuthenticationBloc, AuthenticationState>(
          cubit: authenticationBloc,
          builder: (BuildContext context, AuthenticationState state) {
            if (state is SetUserData) {
              return DefaultTabController(
                length: 2,
                child: BottomNavigationBarWidget(
                    key: key,
                    pagesCache: PagesCache()
                ),
              );
            }
            return Scaffold(
              body: Center(
                child: CircularProgressIndicator(),
              ),
            );
          }));
  }
}
