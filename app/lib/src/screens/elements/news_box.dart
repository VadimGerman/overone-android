// News box contents
import 'package:overone/src/screens/onboarding/news_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:shared/modules/common/models/news_data.dart';

import 'package:flutter/material.dart';
import 'package:flutter_share/flutter_share.dart';
import 'package:shared/modules/content/bloc/bloc_controller.dart';
import 'package:shared/modules/content/bloc/content/content_bloc.dart';
import 'package:shared/modules/content/bloc/content/content_bloc_public.dart';

class NewsBox extends StatefulWidget {
  NewsBox({
    required this.info,
    this.listName = '',
    Key? key}): super(key: key);

  NewsData info;
  final String listName;

  @override
  State<NewsBox> createState() => _NewsBoxState();
}

class _NewsBoxState extends State<NewsBox> {

  final ContentBloc contentBloc = ContentBlocController().contentBloc;

  String getRusMonth(int month) {
    switch (month) {
      case 1: return 'января';
      case 2: return 'февраля';
      case 3: return 'марта';
      case 4: return 'апреля';
      case 5: return 'мая';
      case 6: return 'июня';
      case 7: return 'июля';
      case 8: return 'августа';
      case 9: return 'сентября';
      case 10: return 'октября';
      case 11: return 'ноября';
      case 12: return 'декабря';
    }

    return '';
  }

  String getPutDate() {
    DateTime dateTime = DateTime.fromMillisecondsSinceEpoch(
        (widget.info.putDate).round() * 1000);
    int daysGone = DateTime.now().difference(dateTime).inDays;
    if (daysGone > 1) {
      if (DateTime.now().year > dateTime.year)
        return " ${dateTime.day} ${getRusMonth(dateTime.month)} ${dateTime.year}";
      else
        return " ${dateTime.day} ${getRusMonth(dateTime.month)}";
    }

    String result = '';
    if (daysGone == 1) {
      result = ' Вчера ';
    } else if (daysGone == 0) {
      result = ' Сегодня ';
    }

    return result + "${dateTime.hour}:${dateTime.minute}";
  }

  @override
  Widget build(BuildContext context) {
    var allWidth = MediaQuery.of(context).size.width;
    var bigTextSize = allWidth * 0.05;
    var mainTextSize = allWidth * 0.038;
    var smallTextSize = allWidth * 0.030;

    var image = this.widget.info.imageUrl;
    var hasImage = image != "";
    var sourceName = this.widget.info.source;
    if (sourceName.length > 16) {
      sourceName = sourceName.substring(0, 16).trim() + '...';
    }

    var actionButtonsTitleStyle = TextStyle(
        fontWeight: FontWeight.w400,
        color: Color(0xff151515),
        fontFamily: 'Helvetica Neue',
        fontSize: mainTextSize
    );

    // Single container
    return Container(
        height: allWidth * 0.41,
        padding: EdgeInsets.fromLTRB(0, allWidth * 0.02, 0, 0),
        color: Color(0xfff4f4f4),

        // Inner content box
        child: GestureDetector(
          onTap:  () {
            var articlePath = this.widget.info.link;
            print("Opening " + articlePath);
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        NewsPage(path: articlePath, title: this.widget.info.title)
                ));
            this.contentBloc.add(ArticleAction(
                type: 'AddToList',
                listName: 'history_d3Sk9N',
                articleId: this.widget.info.id
            ));
          },
          child: Container(
            color: Color(0xffffffff),
            child: Stack(
              children: [
                Positioned(
                  left: allWidth * 0.054,
                  top: allWidth * 0.061,
                  width: allWidth * 0.156,
                  height: allWidth * 0.156,
                  child: Container(
                    child: Visibility(
                      visible: hasImage,
                      child: AspectRatio(
                        aspectRatio: 1,
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(4.0),
                            image: DecorationImage(
                              fit: BoxFit.cover,
                              alignment: FractionalOffset.center,
                              image: NetworkImage(image),
                            )
                          )
                        )
                      )
                    )
                  )
                ),
                Positioned(
                  left: allWidth * 0.25,
                  top: allWidth * 0.054,
                  width: allWidth * 0.7,
                  height: allWidth * 0.09,
                  child: Text(
                    this.widget.info.title,
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                      color: Color(0xff151515),
                      fontFamily: 'Helvetica Neue',
                      fontSize: mainTextSize
                    ),
                    maxLines: 2,
                  ),
                ),
                Positioned(
                  left: allWidth * 0.25,
                  top: allWidth * 0.154,
                  width: allWidth * 0.7,
                  height: allWidth * 0.15,
                  child: Text(
                    this.widget.info.description,
                    style: TextStyle(
                        fontWeight: FontWeight.w400,
                        color: Color(0xff979d9f),
                        fontFamily: 'Helvetica Neue',
                        fontSize: mainTextSize
                    ),
                    maxLines: 3,
                  ),
                ),
                Positioned(
                  left: allWidth * 0.25,
                  top: allWidth * 0.33,
                  child: Row(
                    children: [
                      Text(
                        sourceName + '   ',
                        style: TextStyle(
                            fontWeight: FontWeight.w500,
                            color: Color(0xff151515),
                            fontFamily: 'Helvetica Neue',
                            fontSize: smallTextSize
                        ),
                        maxLines: 1,
                      ),
                      Icon(
                        Icons.access_time,
                        color: Color(0xff808486),
                        size: mainTextSize
                      ),
                      Text(
                        getPutDate(),
                        style: TextStyle(
                            fontWeight: FontWeight.w400,
                            color: Color(0xff808486),
                            fontFamily: 'Helvetica Neue',
                            fontSize: smallTextSize
                        ),
                        maxLines: 1,
                      )
                    ],
                  )
                ),
                Positioned(
                  right: allWidth * 0.061,
                  top: allWidth * 0.31,
                  child: InkWell(
                    child: Icon(
                      Icons.more_vert,
                      color: Color(0xff808486),
                      size: bigTextSize,
                    ),
                    onTap: (){
                      showModalBottomSheet(
                          context: context,
                          builder: (context) {
                            return Container(
                              color: Color(0xFF737373),
                              height: this.widget.listName == 'bookmarks_d3Sk9N' ?
                                allWidth * 0.255
                                : allWidth * 0.400,
                              child: Container(
                                decoration: BoxDecoration(
                                  color: Theme.of(context).canvasColor,
                                  borderRadius: BorderRadius.only(
                                    topLeft: const Radius.circular(10),
                                    topRight: const Radius.circular(10),
                                  ),
                                ),
                                child: Column(
                                  children: <Widget>[
                                    Visibility(
                                      visible: this.widget.listName != 'bookmarks_d3Sk9N',
                                      child: InkWell(
                                        onTap: () {
                                          Navigator.pop(context);
                                          this.contentBloc.add(ArticleAction(
                                              type: 'AddToList',
                                              listName: 'bookmarks_d3Sk9N',
                                              articleId: this.widget.info.id
                                          ));
                                        },
                                        child: Container(
                                          height: allWidth * 0.125,
                                          child: ListTile(
                                            leading: Icon(
                                              Icons.bookmark_border,
                                              color: Color(0xff808486),
                                              size: bigTextSize,
                                            ),
                                            title: Text(
                                              'В избранное',
                                              style: actionButtonsTitleStyle,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),

                                    // TODO: implement.
                                    // Container(
                                    //   height: allWidth * 0.125,
                                    //   child: ListTile(
                                    //     leading: Icon(
                                    //       Icons.art_track,
                                    //       color: Color(0xff979d9f),
                                    //       size: bigTextSize,
                                    //     ),
                                    //     title: Text(
                                    //       'Показать похожие',
                                    //       style: actionButtonsTitleStyle,
                                    //     ),
                                    //   ),
                                    // ),
                                    InkWell(
                                      onTap: () {
                                          FlutterShare.share(
                                              title: 'Поделиться статьей',
                                              linkUrl: this.widget.info.link,
                                          );
                                          Navigator.pop(context);
                                      },
                                      child: Container(
                                        height: allWidth * 0.125,
                                        child: ListTile(
                                          leading: Icon(
                                            Icons.share,
                                            color: Color(0xff979d9f),
                                            size: bigTextSize,
                                          ),
                                          title: Text(
                                            'Поделиться',
                                            style: actionButtonsTitleStyle,
                                          ),
                                        ),
                                      ),
                                    ),

                                    Visibility(
                                      visible: this.widget.listName.isEmpty,
                                      child: Container(
                                        height: allWidth * 0.125,
                                        child: ListTile(
                                          leading: Icon(
                                            Icons.thumb_down,
                                            color: Color(0xffff5569),
                                            size: bigTextSize,
                                          ),
                                          title: Text(
                                            'Не интересно',
                                            style: actionButtonsTitleStyle,
                                          ),
                                        ),
                                      ),
                                    ),

                                    Visibility(
                                      visible: this.widget.listName.isNotEmpty,
                                      child: InkWell(
                                        onTap: () {
                                          Navigator.pop(context);
                                          this.contentBloc.add(ArticleAction(
                                              type: 'DeleteFromList',
                                              listName: this.widget.listName,
                                              articleId: this.widget.info.id
                                          ));
                                        },
                                        child: Container(
                                          height: allWidth * 0.125,
                                          child: ListTile(
                                            leading: Icon(
                                              Icons.delete_outline,
                                              color: Color(0xffff5569),
                                              size: bigTextSize,
                                            ),
                                            title: Text(
                                              'Удалить из списка',
                                              style: actionButtonsTitleStyle,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            );
                          }
                      );
                    },
                  )
                )
              ],
            ),
          )
      )
    );
  }
}
