
import 'package:overone/src/screens/elements/subscribe_button.dart';
import 'package:overone/src/screens/elements/underlined_long_button.dart';
import 'package:overone/src/screens/home/folders/folder_selection.dart';
import 'package:overone/src/screens/home/source_articles.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared/modules/common/models/feed_data.dart';
import 'package:shared/modules/content/bloc/bloc_controller.dart';
import 'package:shared/modules/content/bloc/content/content_bloc.dart';
import 'package:shared/modules/content/bloc/content/content_event.dart';
import 'package:shared/modules/content/bloc/content/content_state.dart';

class FeedBox extends StatefulWidget {
  FeedBox({required this.info, this.folderId = -1, Key? key}): super(key: key);

  FeedData info;
  final int folderId;

  @override
  State<FeedBox> createState() => _FeedBoxState();
}

class _FeedBoxState extends State<FeedBox> {
  final ContentBloc contentBloc =
      ContentBlocController().contentBloc;

  @override
  Widget build(BuildContext context) {
    var allWidth = MediaQuery.of(context).size.width;
    var mainTextSize = allWidth * 0.038;
    var smallTextSize = allWidth * 0.030;

    if (this.widget.info == null) {
      return Container();
    }

    return Container(
      height: allWidth * 0.155,
      width: allWidth,
      child: WillPopScope(
        onWillPop: () async => true,
        child: BlocBuilder<ContentBloc, ContentState>(
          cubit: contentBloc,
          builder: (BuildContext context, ContentState state) {
            if (state is SubscribeChanged) {
              var feed = state.singleFeed;
              if (feed.items.isEmpty) {
                // TODO: Catch this or do something else.
                throw "Unreachable";
              } else if (this.widget.info.id == feed.items.first.id) {
                this.widget.info.reset(feed.items.first);
              }
            }

            return InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) =>
                          SourceArticles(
                              this.widget.info,
                              this.widget.folderId
                          )
                  ));
              },
              child: UnderlinedLongButton(
                titleWidget: Column(
                  children: [
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        widget.info.title,
                        style: TextStyle(
                          fontFamily: 'Helvetica Neue',
                          fontSize: mainTextSize,
                          fontWeight: FontWeight.w500,
                          color: Color(0xff151515),
                        ),
                        maxLines: 1,
                      )
                    ),
                    SizedBox(height: allWidth * 0.027),
                    Align(
                      alignment: Alignment.bottomLeft,
                      child: Text(
                        'Прогнозы валютного рынка',//widget.info.description,
                        style: TextStyle(
                          fontFamily: 'Helvetica Neue',
                          fontSize: smallTextSize,
                          fontWeight: FontWeight.w400,
                          color: Color(0xff808486),
                        ),
                        maxLines: 1,
                      )
                    )
                  ]
                ),
                functionalBlockWidth: allWidth * 0.155,
                functionalBlock: SubscribeButton(
                  contentBloc: contentBloc,
                  feedId: widget.info.id,
                  isSubscribed: widget.info.isSubscribed,
                  folderId: widget.folderId,
                ),
                height: allWidth * 0.155,
              ),
            );
          },
        )
      )
    );
  }
}