
import 'package:overone/src/screens/home/articles/articles_screen.dart';

import 'home/categories.dart';
import 'home/folders/folders.dart';
import 'home/main_lenta.dart';
import 'home/profile.dart';

class PagesCache {
  var lentaPage = MainLenta();
  var articlesScreen = ArticlesScreen();
  var categoriesPage = CategoriesList();
  var foldersPage = FoldersList();
  var profilePage = ProfileInfo();
}
