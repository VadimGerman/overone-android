import 'package:api_sdk/api_constants.dart';
import 'package:api_sdk/rest/rest_api_handler_data.dart';

class ApiSdk {
  static loginWithEmailAndPassword(dynamic body) async {
    final response = await RestApiHandlerData.postData(
        '${apiConstants["auth"]}/users/login', body: body);
    return response;
  }

  static signUpWithEmailAndPassword(dynamic body) async {
    final response = await RestApiHandlerData.postData(
        '${apiConstants["auth"]}/users', body: body);
    return response;
  }

  static getUserInfo(int id, Map<String, String> headers) async {
    final response =
        await RestApiHandlerData.getData('${apiConstants["auth"]}/users/$id',
            headers: headers);
    return response;
  }

  static getUserNews(int id, Map<String, String> headers, String params) async {
    final response =
    await RestApiHandlerData.getData('${apiConstants["news"]}/users/$id/news',
        headers: headers, params: params);
    return response;
  }

  static getCategories() async {
    final response =
      await RestApiHandlerData.getData('${apiConstants["news"]}/categories');
    return response;
  }

  static getSubCategories(int parentId) async {
    final response =
      await RestApiHandlerData.getData('${apiConstants["news"]}/categories/${parentId}');
    return response;
  }
  
  static getFolders(Map<String, String> headers, int userId) async {
    final response =
      await RestApiHandlerData.getData('${apiConstants["news"]}/users/${userId}/folders',
        headers: headers);
    return response;
  }

  static getUserListNews(String listName, int id, Map<String, String> headers, String params) async {
    final response =
    await RestApiHandlerData.getData('${apiConstants["news"]}/users/$id/lists/$listName',
        headers: headers, params: params);
    return response;
  }

  static getFeedNews(int feedId, Map<String, String> headers, String params) async {
    final response =
    await RestApiHandlerData.getData('${apiConstants["news"]}/feeds/$feedId/articles',
        headers: headers, params: params);
    return response;
  }

  static getFolderNews(int userId, int folderId, Map<String, String> headers, String params) async {
    final response =
    await RestApiHandlerData.getData('${apiConstants["news"]}/users/$userId/folders/$folderId/news',
        headers: headers, params: params);
    return response;
  }

  static getUserRecomendations(int id, Map<String, String> headers, String params) async {
    final response =
    await RestApiHandlerData.getData('${apiConstants["news"]}/users/$id/recomendations',
        headers: headers, params: params);
    return response;
  }

  static getCategoryFeeds(int categoryId, Map<String, String> headers, String params) async {
    final response =
    await RestApiHandlerData.getData(
        '${apiConstants["news"]}/categories/${categoryId}/feeds',
        headers: headers, params: params);
    return response;
  }

  static getFolderFeeds(int userId, int folderId, Map<String, String> headers) async {
    final response =
    await RestApiHandlerData.getData(
        '${apiConstants["news"]}/users/${userId}/folders/${folderId}/feeds',
        headers: headers);
    return response;
  }

  static getAllUserFeeds(int userId, Map<String, String> headers) async {
    final response =
    await RestApiHandlerData.getData(
        '${apiConstants["news"]}/users/${userId}/folders/feeds',
        headers: headers);
    return response;
  }

  static deleteUserFolder(int userId, int folderId, Map<String, String> headers) async {
    final response =
    await RestApiHandlerData.deleteData(
        '${apiConstants["news"]}/users/${userId}/folders/${folderId}',
        headers: headers);
    return response;
  }

  static changeUserFolder(int userId, Map<String, String> headers, dynamic body) async {
    final response =
    await RestApiHandlerData.putData(
        '${apiConstants["news"]}/users/${userId}/folders',
        headers: headers, body: body);
    return response;
  }

  static createUserFolder(int userId, Map<String, String> headers, dynamic body) async {
    final response =
    await RestApiHandlerData.postData(
        '${apiConstants["news"]}/users/${userId}/folders',
        headers: headers, body: body);
    return response;
  }

  static changeFeedSubscribe(Map<String, String> headers, int userId, dynamic body) async {
    final response = await RestApiHandlerData.postData(
        '${apiConstants["news"]}/users/$userId/folderfeeds',
        headers: headers, body: body);
    return response;
  }

  static articleAction(Map<String, String> headers, int articleId, dynamic body) async {
    final response = await RestApiHandlerData.postData(
        '${apiConstants["news"]}/articles/$articleId',
        headers: headers, body: body);
    return response;
  }
}
