import 'package:api_sdk/main.dart';
import 'dart:convert';
import 'package:crypto/crypto.dart';

class ContentRepository {
  Future<dynamic> getCategories() async {

    final response = await ApiSdk.getCategories();

    return response;
  }

  Future<dynamic> getSubCategories(int parentId) async {

    final response = await ApiSdk.getSubCategories(parentId);

    return response;
  }

  Future<dynamic> getFolders(Map<String, String> headers, int userId) async {

    final response = await ApiSdk.getFolders(headers, userId);

    return response;
  }

  Future<dynamic> getUserNews(int id, Map<String, String> headers, String params) async {

    final response = await ApiSdk.getUserNews(id, headers, params);

    return response;
  }

  Future<dynamic> getFeedNews(int feedId, Map<String, String> headers, String params) async {

    final response = await ApiSdk.getFeedNews(feedId, headers, params);

    return response;
  }

  Future<dynamic> getFolderNews(int userId, int folderId, Map<String, String> headers, String params) async {

    final response = await ApiSdk.getFolderNews(userId, folderId, headers, params);

    return response;
  }

  Future<dynamic> getUserListNews(String listName, int id, Map<String, String> headers, String params) async {

    final response = await ApiSdk.getUserListNews(listName, id, headers, params);

    return response;
  }

  Future<dynamic> getUserRecomendations(int id, Map<String, String> headers, String params) async {

    final response = await ApiSdk.getUserRecomendations(id, headers, params);

    return response;
  }

  Future<dynamic> getCategoryFeeds(int categoryId, Map<String, String> headers, String params) async {

    final response = await ApiSdk.getCategoryFeeds(categoryId, headers, params);

    return response;
  }

  Future<dynamic> getFolderFeeds(int userId, int folderId, Map<String, String> headers) async {

    final response = await ApiSdk.getFolderFeeds(userId, folderId, headers);

    return response;
  }

  Future<dynamic> getAllUserFeeds(int userId, Map<String, String> headers) async {

    final response = await ApiSdk.getAllUserFeeds(userId, headers);

    return response;
  }

  Future<dynamic> deleteUserFolder(int userId, int folderId, Map<String, String> headers) async {

    final response = await ApiSdk.deleteUserFolder(userId, folderId, headers);

    return response;
  }

  Future<dynamic> changeUserFolder(int userId, Map<String, String> headers,
      int folderId, String title, String iconName, bool isPublic) async {
    Map<String, String> data = {
      'id': '$folderId',
      'title': title,
      'iconName': iconName,
      'isPublic': '$isPublic',
    };
    data = {'json': JsonEncoder().convert(data).toString()};

    final response = await ApiSdk.changeUserFolder(userId, headers, data);

    return response;
  }

  Future<dynamic> createUserFolder(int userId, Map<String, String> headers,
      String title, String iconName, bool isPublic) async {
    Map<String, String> data = {
      'title': title,
      'iconName': iconName,
      'isPublic': '$isPublic',
    };
    data = {'json': JsonEncoder().convert(data).toString()};

    final response = await ApiSdk.createUserFolder(userId, headers, data);

    return response;
  }

  Future<dynamic> changeFeedSubscribe(Map<String, String> headers,
      int userId, bool doSubscribe, List<int> folderIds, int feedId) async {

    List<Map<String, String>> folders=[];
    for (int i = 0; i < folderIds.length; i++)
      folders.add({'id': '${folderIds[i]}'});

    Map<String, dynamic> data = {
      'doSubscribe': doSubscribe ? 'true' : 'false',
      'feedId': '$feedId',
    };
    data['folders'] = folders;

    data = {'json': JsonEncoder().convert(data).toString()};
    final response = await ApiSdk.changeFeedSubscribe(headers, userId, data);

    return response;
  }

  Future<dynamic> articleAction(Map<String, String> headers, String type, String listName, int userId, int articleId) async {
    Map<String, String> data = {
      'userId': '$userId',
      'type': type,
      'listName': listName,
    };
    data = {'json': JsonEncoder().convert(data).toString()};
    final response = await ApiSdk.articleAction(headers, articleId, data);

    return response;
  }
}
