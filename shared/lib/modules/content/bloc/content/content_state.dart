import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:shared/modules/common/models/category.dart';
import 'package:shared/modules/common/models/feed_list_data.dart';
import 'package:shared/modules/common/models/folder_info.dart';
import 'package:shared/modules/common/models/news_list_data.dart';

abstract class ContentState extends Equatable {
  const ContentState();

  @override
  List<Object> get props => [];
}

class ContentInitial extends ContentState {}

class ContentLoading extends ContentState {}

class CategoriesBecome extends ContentState {
  final List<CategoryInfo> categories;

  CategoriesBecome({@required this.categories});

  @override
  List<Object> get props => [categories];
}

class SubCategoriesBecome extends ContentState {
  final List<CategoryInfo> categories;

  SubCategoriesBecome({@required this.categories});

  @override
  List<Object> get props => [categories];
}

class FoldersBecome extends ContentState {
  final List<FolderInfo> folders;
  final int allFeedsCount;

  FoldersBecome({@required this.folders, @required this.allFeedsCount});

  @override
  List<Object> get props => [folders, allFeedsCount];
}

class FolderInfoUpdated extends ContentState {
  final FolderInfo folder;

  FolderInfoUpdated({@required this.folder});

  @override
  List<Object> get props => [folder];
}

class FolderInfoCreated extends ContentState {
  final FolderInfo folder;

  FolderInfoCreated({@required this.folder});

  @override
  List<Object> get props => [folder];
}

class NewsBecome extends ContentState {
  final NewsListData news;

  NewsBecome({@required this.news});

  @override
  List<Object> get props => [news];
}

class RecomendationsBecome extends ContentState {
  final NewsListData news;

  RecomendationsBecome({@required this.news});

  @override
  List<Object> get props => [news];
}

class FeedNewsBecome extends ContentState {
  final NewsListData news;

  FeedNewsBecome({@required this.news});

  @override
  List<Object> get props => [news];
}

class UserListNewsBecome extends ContentState {
  final NewsListData news;

  UserListNewsBecome({@required this.news});

  @override
  List<Object> get props => [news];
}

class UserFolderNewsBecome extends ContentState {
  final NewsListData news;

  UserFolderNewsBecome({@required this.news});

  @override
  List<Object> get props => [news];
}

class AllFeedsBecome extends ContentState {
  final FeedListData feeds;

  AllFeedsBecome({@required this.feeds});

  @override
  List<Object> get props => [feeds];
}

class FolderDeleted extends ContentState {
  final int folderId;
  final bool success;

  FolderDeleted({@required this.folderId, this.success});

  @override
  List<Object> get props => [folderId, success];
}

class SubscribeChanged extends ContentState {
  final FeedListData singleFeed;

  SubscribeChanged({@required this.singleFeed});

  @override
  List<Object> get props => [singleFeed];
}

class ArticleActionExecuted extends ContentState {
  ArticleActionExecuted();

  @override
  List<Object> get props => [];
}


class ContentFailure extends ContentState {
  final String message;

  ContentFailure({@required this.message});

  @override
  List<Object> get props => [message];
}
