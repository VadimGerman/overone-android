import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';

abstract class ContentEvent extends Equatable {
  const ContentEvent();

  @override
  List<Object> get props => [];
}

class GetCategories extends ContentEvent {
  @override
  List<Object> get props => [];
}

class GetSubCategories extends ContentEvent {
  final int parentId;

  GetSubCategories({@required this.parentId});

  @override
  List<Object> get props => [];
}

class GetFolders extends ContentEvent {
  @override
  List<Object> get props => [];
}

class GetUserNews extends ContentEvent {
  final int firstId;
  final int lastId;
  final int order;
  final int sort;

  GetUserNews({@required this.firstId, this.lastId, this.order, this.sort});

  @override
  List<Object> get props => [firstId, lastId, order, sort];
}

class GetUserListNews extends ContentEvent {
  final String listName;
  final int lastId;
  final int order;
  final int sort;

  GetUserListNews({@required this.listName, this.lastId, this.order, this.sort});

  @override
  List<Object> get props => [listName, lastId, order, sort];
}

class GetFeedNews extends ContentEvent {
  final int feedId;
  final int firstId;
  final int lastId;
  final int order;
  final int sort;

  GetFeedNews({@required this.feedId, this.firstId, this.lastId, this.order, this.sort});

  @override
  List<Object> get props => [feedId, firstId, lastId, order, sort];
}

class GetFolderNews extends ContentEvent {
  final int folderId;
  final int firstId;
  final int lastId;
  final int order;
  final int sort;

  GetFolderNews({@required this.folderId, this.firstId, this.lastId, this.order, this.sort});

  @override
  List<Object> get props => [folderId, firstId, lastId, order, sort];
}

class GetUserRecomendations extends ContentEvent {
  final int firstId;
  final int lastId;
  final int order;
  final int sort;

  GetUserRecomendations({this.firstId, this.lastId, this.order, this.sort});

  @override
  List<Object> get props => [lastId, order, sort];
}

class GetCategoryFeeds extends ContentEvent {
  final int categoryId;
  final int offset;
  final int order;
  final int sort;

  GetCategoryFeeds({@required this.categoryId, this.offset, this.order, this.sort});

  @override
  List<Object> get props => [categoryId, offset, order, sort];
}

class GetFolderFeeds extends ContentEvent {
  final int folderId;

  GetFolderFeeds({@required this.folderId});

  @override
  List<Object> get props => [folderId];
}

class GetAllUserFeeds extends ContentEvent {
  @override
  List<Object> get props => [];
}

class DeleteUserFolder extends ContentEvent {
  final int folderId;

  DeleteUserFolder({@required this.folderId});

  @override
  List<Object> get props => [folderId];
}

class ChangeUserFolder extends ContentEvent {
  final int folderId;
  final String title;
  final String iconName;
  final bool isPublic;

  ChangeUserFolder({@required this.folderId, this.title, this.iconName, this.isPublic});

  @override
  List<Object> get props => [folderId, title, iconName, isPublic];
}

class CreateUserFolder extends ContentEvent {
  final String title;
  final String iconName;
  final bool isPublic;

  CreateUserFolder({@required this.title, this.iconName, this.isPublic});

  @override
  List<Object> get props => [title, iconName, isPublic];
}

class SubscribeActions extends ContentEvent {
  final bool doSubscribe;
  final List<int> folderIds;
  final int feedId;

  SubscribeActions({@required this.doSubscribe, this.folderIds, this.feedId});

  @override
  List<Object> get props => [doSubscribe, folderIds, feedId];
}

class ArticleAction extends ContentEvent {
  final String type;
  final String listName;
  final int articleId;

  ArticleAction({@required this.type, this.listName, this.articleId});

  @override
  List<Object> get props => [type, listName, articleId];
}
