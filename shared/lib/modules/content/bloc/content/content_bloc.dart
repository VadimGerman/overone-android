import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:shared/main.dart';
import 'package:shared/modules/common/models/category.dart';
import 'package:shared/modules/common/models/feed_list_data.dart';
import 'package:shared/modules/common/models/news_list_data.dart';
import 'package:shared/modules/common/models/folder_info.dart';
import 'package:shared/modules/common/models/pagination_info.dart';

import 'package:shared/modules/content/resources/content_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'package:shared/modules/content/bloc/content/content_event.dart';

import 'content_bloc_public.dart';

class ContentBloc extends Bloc<ContentEvent, ContentState> {
  ContentBloc() : super(ContentInitial());
  final ContentRepository contentService = ContentRepository();

  @override
  Stream<ContentState> mapEventToState(ContentEvent event) async* {
    final SharedPreferences sharedPreferences = await prefs;

    if (event is GetCategories || event is GetSubCategories) {
      yield* _mapCategoriesState(event);
    }

    if (event is GetFolders) {
      yield* _mapFoldersState(event);
    }

    if (event is GetUserNews) {
      yield* _mapUserNewsState(event);
    }

    if (event is GetFeedNews) {
      yield* _mapFeedNewsState(event);
    }

    if (event is GetUserListNews) {
      yield* _mapUserListNewsState(event);
    }

    if (event is GetFolderNews) {
      yield* _mapFolderNews(event);
    }

    if (event is GetUserRecomendations) {
      yield* _mapUserRecomendations(event);
    }

    if (event is GetCategoryFeeds) {
      yield* _mapCategoryFeedsState(event);
    }

    if (event is GetFolderFeeds) {
      yield* _mapFolderFeedsState(event);
    }

    if (event is GetAllUserFeeds) {
      yield* _mapAllUserFeedsState(event);
    }

    if (event is DeleteUserFolder) {
      yield* _mapDeleteUserFolderState(event);
    }

    if (event is ChangeUserFolder) {
      yield* _mapChangeUserFolderState(event);
    }

    if (event is CreateUserFolder) {
      yield* _mapCreateUserFolderState(event);
    }

    if (event is SubscribeActions) {
      yield* _mapChangeSubscribe(event);
    }

    if (event is ArticleAction) {
      yield* _mapArticleAction(event);
    }
  }

  Stream<ContentState> _mapCategoriesState(ContentEvent event) async* {
    final SharedPreferences sharedPreferences = await prefs;
    yield ContentLoading();
    try {
      var data;
      if (event is GetSubCategories) {
        data = await contentService.getSubCategories(event.parentId);
      } else {
        data = await contentService.getCategories();
      }

      if (data["categories"] != null) {
        final categoriesList = data["categories"];
        var result = List<CategoryInfo>();
        for (var item in categoriesList) {
          result.add(CategoryInfo.fromJson(item));
        }

        if (event is GetSubCategories) {
          yield SubCategoriesBecome(categories: result);
        } else {
          yield CategoriesBecome(categories: result);
        }
      } else {
        yield ContentFailure(message: data["message"]);
      }
    } catch (e) {
      yield ContentFailure(
          message: e.toString() ?? 'An unknown error occurred');
    }
  }

  Stream<ContentState> _mapFoldersState(ContentEvent event) async* {
    final SharedPreferences sharedPreferences = await prefs;
    yield ContentLoading();
    try {
      int currentUserId = sharedPreferences.getInt('userId');
      Map<String, String> headers = {
        'authorization': sharedPreferences.getString('authtoken')
      };

      var data = await contentService.getFolders(headers, currentUserId);

      if (data["folders"] != null) {
        final foldersList = data["folders"];
        int allFeedsCount = data["allFeedsCount"];
        var result = List<FolderInfo>();
        for (var item in foldersList) {
          result.add(FolderInfo.fromJson(item));
        }

        yield FoldersBecome(folders: result, allFeedsCount: allFeedsCount);
      } else {
        yield ContentFailure(message: data["message"]);
      }
    } catch (e) {
      yield ContentFailure(
          message: e.toString() ?? 'An unknown error occurred');
    }
  }

  Stream<ContentState> _mapChangeSubscribe(SubscribeActions event) async* {
    final SharedPreferences sharedPreferences = await prefs;
    yield ContentLoading();
    try {
      int currentUserId = sharedPreferences.getInt('userId');
      Map<String, String> headers = {
        'authorization': sharedPreferences.getString('authtoken')
      };

      final data = await contentService.changeFeedSubscribe(headers,
          currentUserId, event.doSubscribe, event.folderIds, event.feedId);

      if (data["feeds"] != null) {
        var result = FeedListData.fromJson(data);

        yield SubscribeChanged(singleFeed: result);
      } else {
        yield ContentFailure(message: data["message"]);
      }
    } catch (e) {
      yield ContentFailure(
          message: e.toString() ?? 'An unknown error occurred');
    }
  }

  Stream<ContentState> _mapArticleAction(ArticleAction event) async* {
    final SharedPreferences sharedPreferences = await prefs;
    yield ContentLoading();
    try {
      int currentUserId = sharedPreferences.getInt('userId');
      Map<String, String> headers = {
        'authorization': sharedPreferences.getString('authtoken')
      };

      final data = await contentService.articleAction(headers,
          event.type, event.listName, currentUserId, event.articleId);

      print("articleAction=");
      print(data);
      if (data["success"] != null && data["success"] == true) {
        yield ArticleActionExecuted();
      } else {
        yield ContentFailure(message: data["message"]);
      }
    } catch (e) {
      yield ContentFailure(
          message: e.toString() ?? 'An unknown error occurred');
    }
  }

  Stream<ContentState> _mapCategoryFeedsState(GetCategoryFeeds event) async* {
    final SharedPreferences sharedPreferences = await prefs;
    yield ContentLoading();
    try {
      int currentUserId = sharedPreferences.getInt('userId');
      Map<String, String> headers = {
        'authorization': sharedPreferences.getString('authtoken')
      };
      var pagination = PaginationInfo(
        limit: 13,
        offset: event.offset,
        order: event.order,
        sort: event.sort,
      );

      final data = await contentService.getCategoryFeeds(
          event.categoryId,
          headers,
          pagination.toString() + "&userid=$currentUserId");

      if (data["feeds"] != null) {
        var result = FeedListData.fromJson(data);

        yield AllFeedsBecome(feeds: result);
      } else {
        yield ContentFailure(message: data["message"]);
      }
    } catch (e) {
      yield ContentFailure(
          message: e.toString() ?? 'An unknown error occurred');
    }
  }

  Stream<ContentState> _mapFolderFeedsState(GetFolderFeeds event) async* {
    final SharedPreferences sharedPreferences = await prefs;
    yield ContentLoading();
    try {
      int currentUserId = sharedPreferences.getInt('userId');
      Map<String, String> headers = {
        'authorization': sharedPreferences.getString('authtoken')
      };

      final data = await contentService.getFolderFeeds(
          currentUserId,
          event.folderId,
          headers);

      if (data["feeds"] != null) {
        var result = FeedListData.fromJson(data);

        yield AllFeedsBecome(feeds: result);
      } else {
        yield ContentFailure(message: data["message"]);
      }
    } catch (e) {
      yield ContentFailure(
          message: e.toString() ?? 'An unknown error occurred');
    }
  }

  Stream<ContentState> _mapAllUserFeedsState(GetAllUserFeeds event) async* {
    final SharedPreferences sharedPreferences = await prefs;
    yield ContentLoading();
    try {
      int currentUserId = sharedPreferences.getInt('userId');
      Map<String, String> headers = {
        'authorization': sharedPreferences.getString('authtoken')
      };

      final data = await contentService.getAllUserFeeds(
          currentUserId,
          headers);

      if (data["feeds"] != null) {
        var result = FeedListData.fromJson(data);

        yield AllFeedsBecome(feeds: result);
      } else {
        yield ContentFailure(message: data["message"]);
      }
    } catch (e) {
      yield ContentFailure(
          message: e.toString() ?? 'An unknown error occurred');
    }
  }

  Stream<ContentState> _mapDeleteUserFolderState(DeleteUserFolder event) async* {
    final SharedPreferences sharedPreferences = await prefs;
    yield ContentLoading();
    try {
      int currentUserId = sharedPreferences.getInt('userId');
      Map<String, String> headers = {
        'authorization': sharedPreferences.getString('authtoken')
      };

      final data = await contentService.deleteUserFolder(currentUserId, event.folderId, headers);

      if (data["success"] != null && data["success"] == true) {
        yield FolderDeleted(folderId: data["folderId"], success: data["success"]);
      } else {
        yield ContentFailure(message: data["message"]);
      }
    } catch (e) {
      yield ContentFailure(
          message: e.toString() ?? 'An unknown error occurred');
    }
  }

  Stream<ContentState> _mapChangeUserFolderState(ChangeUserFolder event) async* {
    final SharedPreferences sharedPreferences = await prefs;
    yield ContentLoading();
    try {
      int currentUserId = sharedPreferences.getInt('userId');
      Map<String, String> headers = {
        'authorization': sharedPreferences.getString('authtoken')
      };

      final data = await contentService.changeUserFolder(currentUserId, headers,
          event.folderId, event.title, event.iconName, event.isPublic);

      if (data["folder"] != null) {
        FolderInfo folder = FolderInfo.fromJson(data["folder"]);

        yield FolderInfoUpdated(folder: folder);
      } else {
        yield ContentFailure(message: data["message"]);
      }
    } catch (e) {
      yield ContentFailure(
          message: e.toString() ?? 'An unknown error occurred');
    }
  }

  Stream<ContentState> _mapCreateUserFolderState(CreateUserFolder event) async* {
    final SharedPreferences sharedPreferences = await prefs;
    yield ContentLoading();
    try {
      int currentUserId = sharedPreferences.getInt('userId');
      Map<String, String> headers = {
        'authorization': sharedPreferences.getString('authtoken')
      };

      final data = await contentService.createUserFolder(currentUserId, headers,
          event.title, event.iconName, event.isPublic);

      if (data["folder"] != null) {
        FolderInfo folder = FolderInfo.fromJson(data["folder"]);

        yield FolderInfoCreated(folder: folder);
      } else {
        yield ContentFailure(message: data["message"]);
      }
    } catch (e) {
      yield ContentFailure(
          message: e.toString() ?? 'An unknown error occurred');
    }
  }

  Stream<ContentState> _mapUserNewsState(GetUserNews event) async* {
    final SharedPreferences sharedPreferences = await prefs;
    yield ContentLoading();
    try {
      int currentUserId = sharedPreferences.getInt('userId');
      Map<String, String> headers = {
        'authorization': sharedPreferences.getString('authtoken')
      };
      var pagination = PaginationInfo(
        limit: 13,
        firstId: event.firstId,
        lastId: event.lastId,
        order: event.order,
        sort: event.sort,
      );

      final data = await contentService.getUserNews(currentUserId,
          headers,
          pagination.toString());

      if (data["news"] != null) {
        var result = NewsListData.fromJson(data);

        yield NewsBecome(news: result);
      } else {
        yield ContentFailure(message: data["message"]);
      }
    } catch (e) {
      yield ContentFailure(
          message: e.toString() ?? 'An unknown error occurred');
    }
  }

  Stream<ContentState> _mapFeedNewsState(GetFeedNews event) async* {
    final SharedPreferences sharedPreferences = await prefs;
    yield ContentLoading();
    try {
      var pagination = PaginationInfo(
        limit: 13,
        firstId: event.firstId,
        lastId: event.lastId,
        order: event.order,
        sort: event.sort,
      );

      final data = await contentService.getFeedNews(event.feedId,
          {},
          pagination.toString());

      if (data["news"] != null) {
        var result = NewsListData.fromJson(data);

        yield FeedNewsBecome(news: result);
      } else {
        yield ContentFailure(message: data["message"]);
      }
    } catch (e) {
      yield ContentFailure(
          message: e.toString() ?? 'An unknown error occurred');
    }
  }

  Stream<ContentState> _mapUserListNewsState(GetUserListNews event) async* {
    final SharedPreferences sharedPreferences = await prefs;
    yield ContentLoading();
    try {
      int currentUserId = sharedPreferences.getInt('userId');
      Map<String, String> headers = {
        'authorization': sharedPreferences.getString('authtoken')
      };
      var pagination = PaginationInfo(
        limit: 13,
        lastId: event.lastId,
        order: event.order,
        sort: event.sort,
      );

      final data = await contentService.getUserListNews(event.listName,
          currentUserId,
          headers,
          pagination.toString());

      if (data["news"] != null) {
        var result = NewsListData.fromJson(data);

        yield UserListNewsBecome(news: result);
      } else {
        yield ContentFailure(message: data["message"]);
      }
    } catch (e) {
      yield ContentFailure(
          message: e.toString() ?? 'An unknown error occurred');
    }
  }

  Stream<ContentState> _mapFolderNews(GetFolderNews event) async* {
    final SharedPreferences sharedPreferences = await prefs;
    yield ContentLoading();
    try {
      int currentUserId = sharedPreferences.getInt('userId');
      Map<String, String> headers = {
        'authorization': sharedPreferences.getString('authtoken')
      };
      var pagination = PaginationInfo(
        limit: 13,
        firstId: event.firstId,
        lastId: event.lastId,
        order: event.order,
        sort: event.sort,
      );

      final data = await contentService.getFolderNews(
          currentUserId,
          event.folderId,
          headers,
          pagination.toString());

      if (data["news"] != null) {
        var result = NewsListData.fromJson(data);

        yield UserFolderNewsBecome(news: result);
      } else {
        yield ContentFailure(message: data["message"]);
      }
    } catch (e) {
      yield ContentFailure(
          message: e.toString() ?? 'An unknown error occurred');
    }
  }

  Stream<ContentState> _mapUserRecomendations(GetUserRecomendations event) async* {
    final SharedPreferences sharedPreferences = await prefs;
    yield ContentLoading();
    try {
      int currentUserId = sharedPreferences.getInt('userId');
      Map<String, String> headers = {
        'authorization': sharedPreferences.getString('authtoken')
      };
      var pagination = PaginationInfo(
        limit: 13,
        firstId: event.firstId,
        lastId: event.lastId,
        order: event.order,
        sort: event.sort,
      );

      final data = await contentService.getUserRecomendations(currentUserId,
          headers,
          pagination.toString());

      if (data["news"] != null) {
        var result = NewsListData.fromJson(data);

        yield RecomendationsBecome(news: result);
      } else {
        yield ContentFailure(message: data["message"]);
      }
    } catch (e) {
      yield ContentFailure(
          message: e.toString() ?? 'An unknown error occurred');
    }
  }
}
