import 'package:shared/main.dart';
import 'package:shared/modules/content/bloc/content/content_bloc.dart';

class ContentBlocController {
  ContentBlocController._();
  static ContentBlocController _instance =
  ContentBlocController._();
  factory ContentBlocController() => _instance;

  // ignore: close_sinks
  ContentBloc contentBloc = ContentBloc();
}
