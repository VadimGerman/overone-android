import 'news_data.dart';

// TODO: rename.
enum ListTypes {
  recomendations,
  mainlenta,
  userlist,
  folderlenta,
  feedlenta,
  none,
}

class NewsListData {
  ListTypes type;
  List<NewsData> items = [];
  String listName;
  int feedId;
  int folderId;
  bool hasUpdates;

  NewsListData({this.type,
    this.items,
    this.listName='',
    this.feedId=-1,
    this.folderId=-1,
    this.hasUpdates=false});

  NewsListData.fromJson(Map<String, dynamic> json) {
    final newsList = json['news'];
    for (var item in newsList) {
      items.add(NewsData.fromJson(item));
    }

    var typeString = json['type'];
    type = ListTypes.values.firstWhere((e) => e.toString().split('.')[1] == typeString);
    listName = json['listname'];
    feedId = json['feedid'];
    folderId = json['folderid'];
    hasUpdates = json['hasUpdates'];
  }
}
