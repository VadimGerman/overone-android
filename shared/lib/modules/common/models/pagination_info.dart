class PaginationInfo {
  int limit;
  int offset = -1;
  int firstId = -1;
  int lastId = -1;
  int sort;
  int order = 0;

  PaginationInfo({this.limit, this.offset, this.firstId, this.lastId, this.sort, this.order});

  String toString() {
    return 'limit=$limit&offset=$offset&sort=$sort&order=$order&firstId=$firstId&lastId=$lastId';
  }
}
