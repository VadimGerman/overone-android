
import 'feed_data.dart';

enum FeedListTypes {
  categoryfeeds,
  folderfeeds,
  alluserfeeds,
  subscribeaction,  // for single feed when you subscribe/unsubscrabe.
  none,
}

class FeedListData {
  FeedListTypes type;
  List<FeedData> items = [];
  int userId;
  int folderId;
  int categoryId;

  FeedListData({this.type,
    this.items,
    this.userId=-1,
    this.folderId=-1,
    this.categoryId=-1});

  FeedListData.fromJson(Map<String, dynamic> json) {
    final feedList = json['feeds'];
    for (var item in feedList) {
      items.add(FeedData.fromJson(item));
    }

    var typeString = json['type'];
    type = FeedListTypes.values.firstWhere((e) => e.toString().split('.')[1] == typeString);
    userId = json['userId'];
    folderId = json['folderId'];
    categoryId = json['categoryId'];
  }
}
